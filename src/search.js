// React
import React, {Component} from 'react';

import Users from './user'

export default class Search extends Component {
    state = {
        repos: 0,
        location: "Russia",
        language: "JavaScript"
    };

    paramsChanged = (e) => {
        this.setState({[e.target.id]: e.target.value})
    };

    render() {
        return (
            <div>
                <div className="ui large inverted stackable menu container">
                    <div className="item">
                        <div className="ui input">
                            <input value={this.state.repos} id="repos" type="number" placeholder="Repos count" onChange={this.paramsChanged}/>
                        </div>
                    </div>

                    <div className="item">
                        <div className="ui input">
                            <input value={this.state.location} id="location" type="text" placeholder="Location" onChange={this.paramsChanged}/>
                        </div>
                    </div>

                    <div className="item">
                        <div className="ui input">
                            <input value={this.state.language} id="language" type="text" placeholder="Language" onChange={this.paramsChanged}/>
                        </div>
                    </div>
                </div>
                <Users repos={this.state.repos} location={this.state.location} language={this.state.language}/>
            </div>
        )
    }
}