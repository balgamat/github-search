// React
import React, {Component} from 'react'

// Apollo
import {ApolloProvider} from 'react-apollo'
import ApolloClient, {createNetworkInterface} from 'apollo-client'

// Auth
import {login, config} from './githubLogin'

// App.Components
import Search from './search'

// Global.Auth
let TOKEN = null;

// Global.Apollo
const networkInterface = createNetworkInterface('https://api.github.com/graphql');

networkInterface.use([
    {
        applyMiddleware(req, next) {
            if (!req.options.headers) {
                req.options.headers = {} // Create the header object if needed.
            }

            // Send the login token in the Authorization header
            req.options.headers.authorization = `Bearer ${TOKEN}`;
            next();
        }
    }
]);

const client = new ApolloClient({
    networkInterface
});

// App
export default class App extends Component {
    constructor() {
        super();
        this.state = {login: false}
    }

    componentDidMount() {
        login(config.USERNAME, config.PASSWORD).then(token => {
            TOKEN = token;
            this.setState({login: true});
        })
    }

    render() {
        // Log in state
        if (!this.state.login) {
            return <p>Loading...</p>
        }

        // Logged in, fetch from Github
        return this.state.login
            ? <ApolloProvider client={client}>
                <Search/>
            </ApolloProvider>
            : <p className="ui header">Logging in...</p>
    }
}
