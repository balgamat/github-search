// React
import React from 'react'

// GraphQL
import gql from 'graphql-tag';
import {graphql} from 'react-apollo';

const GetUsersQuery = gql`
  query UsersSearch($query: String!) {
      search(query: $query, type: USER, first: 100) {
        userCount
        edges {
            node {
                ... on User {
                    login
                    location
                }
            }
        }
      }
  }
`;

const withInfo = graphql(GetUsersQuery, {
    options: ({repos, location, language}) => {
        console.log(`repos`, repos.toString());
        const query = `repos:>${repos} location:${location} language:${language}`;
        return {
            variables: {
                query
            }
        }
    },
    props: ({data}) => {
        // loading state
        if (data.loading) {
            return {loading: true};
        }

        // error state
        if (data.error) {
            console.error(data.error);
        }

        // OK state
        return {data};
    },
});

// Users
class Users extends React.Component {
    render() {
        if (this.props.data) {
            return (<div>
                <h2 className="ui header">Users found: {this.props.data.search.userCount}</h2>
                <ul className="ui list">
                    {/*Map the result to a list*/}
                    {this.props.data.search.edges.map(({node}) => (
                        <li className="ui user" key={node.login}><h3>{node.login}</h3>{node.location}</li>
                    ))}
                </ul>
            </div>)
        } else {
            return(<h2 className="ui header">Loading...</h2>);
        }
    }
}

const UsersWithInfo = withInfo(Users);
export default UsersWithInfo;
