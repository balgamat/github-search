const base64 = require('base-64');

export const config = {
    USERNAME: 'findthemall',
    PASSWORD: 'rtfasf044_328?"',
    GITHUB_CLIENT_ID: '11ce23e80d88aa886424',
    GITHUB_CLIENT_SECRET: 'b00fbc107b8d6d9932942c4de07cff825796d4eb'
};

const AUTH_URL_PATH = 'https://api.github.com/authorizations'

export function login (name, pwd) {
  const bytes = name.trim() + ':' + pwd.trim()
  const encoded = base64.encode(bytes)

  return fetch(AUTH_URL_PATH, {
    method: 'POST',
    headers: {
      Authorization: 'Basic ' + encoded,
      'User-Agent': 'GitHub Issue Browser',
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/vnd.github.inertia-preview+json'
    },
    body: JSON.stringify({
      client_id: config.GITHUB_CLIENT_ID,
      client_secret: config.GITHUB_CLIENT_SECRET,
      scopes: ['user', 'repo'],
      note: 'not abuse'
    })
  }).then(response =>
    response.json().then(json => {
      if (response.status < 400) {
        return json.token
      } else {
        throw new Error(json.message)
      }
    })
  )
}
